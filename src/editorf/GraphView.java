/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorf;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.view.mxGraph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author kynew
 */
public class GraphView extends mxGraph {

    private mxCell editCell;
    private Map values = new HashMap();
    private ArrayList editCells = new ArrayList();

    private int minSize = 20;

    public GraphView() {

        this.model.addListener(mxEvent.START_EDITING, new mxIEventListener() {

            @Override
            public void invoke(Object arg0, mxEventObject arg1) {
                editCell = (mxCell) arg0;
            }
        });

        this.model.addListener(mxEvent.END_UPDATE, new mxIEventListener() {

            @Override
            public void invoke(Object arg0, mxEventObject arg1) {
                if (editCell != null) {
                    String value = (String) editCell.getValue();

                    if (value.length() > minSize) {
                        values.put(editCell.getId(), value);
                        editCell.setValue(value.subSequence(0, minSize - 3) + "...");
                        editCells.add(editCell);
                    }
                }
            }
        });

    }

    public boolean isCellContains(Object cell) {
        if (editCells.contains(cell)) {
            return true;
        }
        return false;
    }

    public void setFullText(mxCell cell) {
        if (values.get(cell.getId()) != null) {
            cell.setValue(values.get(cell.getId()));
        }
    }

    @Override
    public boolean isCellEditable(Object cell) {
        editCell = (mxCell) cell;
        setFullText(editCell);
        return true;
    }
}
