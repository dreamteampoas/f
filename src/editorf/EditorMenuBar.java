/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package editorf;

import com.mxgraph.swing.util.mxGraphActions;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.TransferHandler;

/**
 *
 * @author user
 */
public class EditorMenuBar extends JMenuBar
{
    public EditorMenuBar(final EditorF editor)
    {
        // Menu panel File
        JMenu file = new JMenu("Файл");
        add(file);
        
        file.add(editor.bind("Создать", new Actions.CreateAction(), "/images/menu/new.gif"));
        file.add(editor.bind("Открыть", new Actions.OpenAction(), "/images/menu/open.gif"));
        file.addSeparator();
        file.add(editor.bind("Сохранить", new Actions.SaveAction(false), "/images/menu/save.gif"));
        file.add(editor.bind("Сохранить как", new Actions.SaveAction(true), "/images/menu/saveas.gif"));
        file.addSeparator();
        file.add(editor.bind("Экспортировать", new Actions.SaveAction(true, true), "/images/menu/export.gif"));
        file.addSeparator();
        file.add(editor.bind("Выход", new Actions.ExitAction()));
        
        // Menu panel Edit
        JMenu edit = new JMenu("Правка");
        add(edit);
        
        edit.add(editor.bind("Удалить", mxGraphActions.getDeleteAction(), "/images/menu/delete.gif"));
        edit.addSeparator();
        edit.add(editor.bind("Отменить", new Actions.HistoryAction(true), "/images/menu/undo.gif"));
        edit.add(editor.bind("Повторить", new Actions.HistoryAction(false), "/images/menu/redo.gif"));
        edit.addSeparator();
        edit.add(editor.bind("Вырезать", TransferHandler.getCutAction(), "/images/menu/cut.gif"));
        edit.add(editor.bind("Копировать", TransferHandler.getCopyAction(), "/images/menu/copy.gif"));
        edit.add(editor.bind("Вставить", TransferHandler.getPasteAction(), "/images/menu/paste.gif"));
        
        // Menu panel View
        JMenu view = new JMenu("Вид");
        add(view);
        
        view.add(editor.bind("Увеличить", mxGraphActions.getZoomInAction(), "/images/menu/zoomin.gif"));
        view.add(editor.bind("Уменьшить", mxGraphActions.getZoomOutAction(), "/images/menu/zoomout.gif"));
    }
}
