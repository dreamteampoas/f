/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorf;

import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMouseAdapter;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUndoManager;
import com.mxgraph.util.mxUndoableEdit;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.Object;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import org.w3c.dom.Document;

public class EditorF extends JPanel {

    public static final NumberFormat numberFormat = NumberFormat.getInstance();

    protected JPanel libraryPane, left;
    protected int sizeElements = 100;               // Размер элемента на диаграмме по-умолчанию
    protected int minSize = 50;                     // Минимальный размер элемента (ширина или высота)
    protected int maxSize = 250;                    // Максимальный размер элемента (ширина или высота)
    private Map <String, Integer> cellSizes
            = new HashMap <String, Integer> ();     // Размеры событий и шлюзов для сохранения пропорций 1:1
    
    private String appTitle = "EditorF";            // Заголовок приложения
    private File currentFile;                       // Текущий используемый файл
    private boolean modified = false;               // Признак наличия изменений в текущей диаграмме
    private mxGraphComponent graphComponent;        // Рабочее поле редактора
    private mxUndoManager undoManager;              // Менеджер, отвечающий за отмену и повторение последних действий
    private KeyboardHandler keyboardHandler;        // Обработчик горячих клавиш
    private mxRubberband rubberband;                // Менеджер множественного выделения мышью
    
    private List <String> movedToBackAnnotations = new ArrayList <String> ();
    
    private mxEventSource.mxIEventListener changeTracker = new mxEventSource.mxIEventListener() {
        public void invoke(Object source, mxEventObject evt) {
            setModified(true);
        }
    };                                              // Менеджер, контролирующий
    protected mxEventSource.mxIEventListener undoHandler = new mxEventSource.mxIEventListener() {
        public void invoke(Object source, mxEventObject evt) {
            undoManager.undoableEditHappened((mxUndoableEdit) evt.getProperty("edit"));
        }
    };

    public EditorF() {
        this("mxGraph Editor", new CustomGraphComponent(new GraphView()));
    }
    
    public EditorF(String appTitle, mxGraphComponent component) {
        graphComponent = component;
        final mxGraph graph = graphComponent.getGraph();
        undoManager = new mxUndoManager();
        graphComponent.getConnectionHandler().setCreateTarget(false);
        graph.setSplitEnabled(false);
        
        
        graphComponent.getGraphControl().addMouseMotionListener(new mxMouseAdapter() {
                @Override
                public void mouseMoved(MouseEvent e)
                {
                    mxCell cell = (mxCell) graphComponent.getCellAt(e.getX(), e.getY(), false);
                    if(((GraphView)graph).isCellContains(cell)){
                        ((GraphView)graph).setFullText(cell);
                    }
                }
            }
        );
        
        // Updates the modified flag if the graph model changes
        graph.getModel().addListener(mxEvent.CHANGE, changeTracker);
        graph.getModel().addListener(mxEvent.END_UPDATE, new mxIEventListener(){
            @Override
            public void invoke(Object o, mxEventObject eo) {
                for (Map.Entry<String, Object> entry : ((mxGraphModel)o).getCells().entrySet())
                {
                    mxCell c = (mxCell)entry.getValue();
                    if (c.isVertex())
                    {
                        if (c.getStyle().contains("annotation") && !movedToBackAnnotations.contains(c.getId())) {
                            c.setConnectable(false);
                            movedToBackAnnotations.add(c.getId());
                            Object[] cell = new Object[1];
                            cell[0] = c;
                            graph.orderCells(true, cell);
                        }
                        // Контроль пропорций событий и шлюзов
                        if (c.getStyle().contains("general") || c.getStyle().contains("rhombus")) {
                            if (cellSizes.containsKey(c.getId())) {
                                mxGeometry g = c.getGeometry();
                                
                                // Изменилась высотка
                                if (g.getWidth() == cellSizes.get(c.getId()) && 
                                        g.getHeight() != cellSizes.get(c.getId()))
                                    g.setWidth(g.getHeight());
                                // Изменилась ширина
                                else if (g.getHeight() == cellSizes.get(c.getId()) && 
                                        g.getWidth() != cellSizes.get(c.getId()))
                                    g.setHeight(g.getWidth());
                                // Изменились обе стороны
                                else if (g.getHeight() != cellSizes.get(c.getId()) && 
                                        g.getWidth() != cellSizes.get(c.getId())) {
                                    Integer size = (int)Math.min(g.getWidth(), g.getHeight());
                                    g.setHeight(size);
                                    g.setWidth(size);
                                }
                            }
                            cellSizes.put(c.getId(), (int)c.getGeometry().getWidth());
                        }
                        
                        // Контроль минимальных и максимальных размеров
                        if (c.getGeometry().getHeight() < minSize)
                            c.getGeometry().setHeight(minSize);
                        if (c.getGeometry().getWidth() < minSize)
                            c.getGeometry().setWidth(minSize);
                        if (!(c.getStyle().contains("pool") || c.getStyle().contains("annotation"))) {
                            if (c.getGeometry().getHeight() > maxSize)
                                c.getGeometry().setHeight(maxSize);
                            if (c.getGeometry().getWidth() > maxSize)
                                c.getGeometry().setWidth(maxSize);
                        }
                    }
                }
            }
        });
        
        // Adds the command history to the model and view
        graph.getModel().addListener(mxEvent.UNDO, undoHandler);
        graph.getView().addListener(mxEvent.UNDO, undoHandler);
        
        // Keeps the selection in sync with the command history
        mxEventSource.mxIEventListener undoHandler = new mxEventSource.mxIEventListener() {
            public void invoke(Object source, mxEventObject evt) {
                List<mxUndoableEdit.mxUndoableChange> changes = ((mxUndoableEdit) evt.getProperty("edit")).getChanges();
                graph.setSelectionCells(graph.getSelectionCellsForChanges(changes));
            }
        };

        undoManager.addListener(mxEvent.UNDO, undoHandler);
        undoManager.addListener(mxEvent.REDO, undoHandler);

        // Creates the library pane that contains the tabs with the palettes
        libraryPane = new JPanel();
        libraryPane.setLayout(new BoxLayout(libraryPane,BoxLayout.Y_AXIS));
        
        final JScrollPane scrollPane = new JScrollPane(libraryPane);
        scrollPane
        .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane
        .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        left = new JPanel();
        left.setLayout(new GridLayout(1,1));
        left.setMinimumSize(new Dimension(220,0));
        left.add(scrollPane);

        // Creates the outer split pane that contains the inner split pane and
        // the graph component on the right side of the window
        JSplitPane outer = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, left, graphComponent);
        outer.setOneTouchExpandable(true);
        outer.setDividerLocation(250);
        outer.setDividerSize(6);
        outer.setBorder(null);
        // Puts everything together
        setLayout(new BorderLayout());
        add(outer, BorderLayout.CENTER);

	// Сигналы клавиатуры
        keyboardHandler = new KeyboardHandler(graphComponent);
        rubberband = new mxRubberband(graphComponent);
        installMouseWheelListener();
        
        // Установить начальный заголовок окна
        updateTitle();

        // Создание и заполнение панелей элементов 
        Palette palette = insertPalette("Процессы",1); // название и кол-во элементов в панели
        Palette palette1 = insertPalette("События",3);
        Palette palette2 = insertPalette("Шлюзы",3);
        Palette palette3 = insertPalette("Потоки",2);
        Palette palette4 = insertPalette("Пулы",1);
        Palette palette5 = insertPalette("Аннотации",1);
        
        // название, иконка, стиль, размеры, подпись
        palette
                .addTemplate(
                        "Процесс",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/task.png")),
                        "process;image=/images/elements/task.png",
                        sizeElements, sizeElements, "Процесс");
        palette1
                .addTemplate(
                        "Событие1",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/event1.png")),
                        "generalStart;image=/images/elements/event1.png",
                        sizeElements, sizeElements, "");
        palette1
                .addTemplate(
                        "Событие2",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/event2.png")),
                        "generalIntermediate;image=/images/elements/event2.png",
                        sizeElements, sizeElements, "");
        palette1
                .addTemplate(
                        "Событие3",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/event3.png")),
                        "generalFinish;image=/images/elements/event3.png",
                        sizeElements, sizeElements, "");
        palette2
                .addTemplate(
                        "Параллельный шлюз",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/pargateway.png")),
                        "rhombusImage;rounded=1;image=/images/elements/pargateway.png",
                        sizeElements, sizeElements, "");
        palette2
                .addTemplate(
                        "Эксклюзивный шлюз",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/excgateway.png")),
                        "rhombusImage;rounded=1;image=/images/elements/excgateway.png",
                        sizeElements, sizeElements, "");
        palette2
                .addTemplate(
                        "Неэксклюзивный шлюз",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/nexcgateway.png")),
                        "rhombusImage;rounded=1;image=/images/elements/nexcgateway.png",
                        sizeElements, sizeElements, "");
        palette3
                .addEdgeTemplate(
                        "Поток управления",
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/controlflow.png")),
                        "image=/images/elements/controlflow.png",
                        sizeElements, sizeElements, "");
        palette3
                .addEdgeTemplate(
                        "Ассоциация",                      
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/association.png")),
                        "assosiationEdge;image=/images/elements/association.png",
                        sizeElements, sizeElements, "");    
        palette4
                .addTemplate(
                        "Свернутый пул",                      
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/pull.png")),
                        "pool;image=/images/elements/pull.png",
                        sizeElements, sizeElements, "Свернутый пул");   
        palette5
                .addTemplate(
                        "Текстовая аннотация",                      
                        new ImageIcon(
                                EditorF.class
                                .getResource("/images/elements/textannotate.png")),
                        "annotation;image=/images/elements/textannotate.png",
                        sizeElements, sizeElements, "Текстовая аннотация");    
}

    public Palette insertPalette(String title, int count) {
        Palette palette = new Palette();
        palette.setTitle(title);
        palette.countElements = count;
        palette.setLayout(new GridLayout(palette.countElements,1));
        libraryPane.add(palette);
        
        // Updates the widths of the palettes if the container size changes
        libraryPane.addComponentListener(new ComponentAdapter() {
            /**
             *
             */
            public void componentResized(ComponentEvent e) {
                palette.setPreferredWidth(libraryPane.getWidth()-15);
            }

        });

        return palette;
    }

    public void setModified(boolean modified) {
        boolean oldValue = this.modified;
        this.modified = modified;

        firePropertyChange("modified", oldValue, modified);

        if (oldValue != modified) {
            updateTitle();
        }
    }

    /**
     * 
     * @return whether or not the current graph has been modified
     */
    public boolean isModified()
    {
        return modified;
    }

    public void updateTitle() {
        JFrame frame = (JFrame) SwingUtilities.windowForComponent(this);

        if (frame != null) {
            String title = (currentFile != null) ? currentFile.getAbsolutePath() : mxResources.get("newDiagram");

            if (modified) {
                title += "*";
            }

            frame.setTitle(title + " - " + appTitle);
        }
    }

    public JFrame createFrame(JMenuBar menuBar) {
        JFrame frame = new JFrame();
        frame.getContentPane().add(this);        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(menuBar);
        frame.setSize(870, 640);
        frame.setMinimumSize(new Dimension(500,500));
        // Обновить заголовок окна
        updateTitle();

        return frame;
    }

    public static class CustomGraphComponent extends mxGraphComponent {

        public CustomGraphComponent(mxGraph graph) {
            super(graph);

            // Sets switches typically used in an editor
            setGridVisible(true);
            setToolTips(true);
            graph.setAutoOrigin(true);
            getConnectionHandler().setCreateTarget(true);

            // Loads the defalt stylesheet from an external file
            mxCodec codec = new mxCodec();
            Document doc = mxUtils.loadDocument(EditorF.class.getResource(
                            "/resources/default-style.xml")
                            .toString());
            codec.decode(doc.getDocumentElement(), graph.getStylesheet());
            
            // Sets the background to white
            getViewport().setOpaque(true);
            getViewport().setBackground(Color.WHITE);
        }
    }
	
    /**
     * 
     */
    public mxGraphComponent getGraphComponent()
    {
        return graphComponent;
    }

    /**
     * 
     * @param name
     * @param action
     * @return a new Action bound to the specified string name
     */
    public Action bind(String name, final Action action)
    {
        return bind(name, action, null);
    }

    /**
     * 
     * @param name
     * @param action
     * @return a new Action bound to the specified string name and icon
     */
    @SuppressWarnings("serial")
    public Action bind(String name, final Action action, String iconUrl)
    {
        AbstractAction newAction = new AbstractAction(name, (iconUrl != null) ? new ImageIcon(
                        EditorF.class.getResource(iconUrl)) : null)
        {
            public void actionPerformed(ActionEvent e)
            {
                action.actionPerformed(new ActionEvent(getGraphComponent(), e
                                .getID(), e.getActionCommand()));
            }
        };

        newAction.putValue(Action.SHORT_DESCRIPTION, action.getValue(Action.SHORT_DESCRIPTION));

        return newAction;
    }

    /**
     * 
     */
    public mxUndoManager getUndoManager()
    {
        return undoManager;
    }

    /**
     * 
     */
    public File getCurrentFile()
    {
            return currentFile;
    }
    
    /**
     * 
     */
    public void setCurrentFile(File file)
    {
        File oldValue = currentFile;
        currentFile = file;

        firePropertyChange("currentFile", oldValue, file);

        if (oldValue != file)
        {
            updateTitle();
        }
    }
    
    public void installMouseWheelListener()
    {
        // Installs mouse wheel listener for zooming
        MouseWheelListener wheelTracker = new MouseWheelListener()
        {
            /**
             * 
             */
            public void mouseWheelMoved(MouseWheelEvent e)
            {
                if (e.isControlDown())
                {
                    EditorF.this.mouseWheelMoved(e);
                }
            }
        };
        graphComponent.addMouseWheelListener(wheelTracker);
    }

    /**
     * 
     */
    protected void mouseWheelMoved(MouseWheelEvent e)
    {
        if (e.getWheelRotation() < 0)
        {
            graphComponent.zoomIn();
        }
        else
        {
            graphComponent.zoomOut();
        }
    }

    /**
     * 
     */
    public void exit()
    {
        JFrame frame = (JFrame) SwingUtilities.windowForComponent(this);

        if (frame != null)
        {
            frame.dispose();
        }
    }
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        mxSwingConstants.SHADOW_COLOR = Color.LIGHT_GRAY;
        mxConstants.W3C_SHADOWCOLOR = "#D3D3D3";

        EditorF editor = new EditorF();
        editor.createFrame(new EditorMenuBar(editor)).setVisible(true);
    }
}
