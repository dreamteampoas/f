/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.TransferHandler;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.swing.util.mxGraphTransferable;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.Icon;
import org.jdesktop.swingx.JXTaskPane;

public class Palette extends JXTaskPane {

    protected JLabel selectedEntry = null;

    protected mxEventSource eventSource = new mxEventSource(this);

    protected int countElements = 0;
    
    protected ArrayList<JLabel> elements = new ArrayList();
    protected ArrayList<ImageIcon> icons = new ArrayList();
    protected JLabel label = new JLabel();
    protected Dimension d = new Dimension();

    @SuppressWarnings("serial")
    public Palette() {
        setBackground(Color.WHITE);
        
        // Clears the current selection when the background is clicked
        addMouseListener(new MouseListener() {

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
             */
            public void mousePressed(MouseEvent e) {
                clearSelection();
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
             */
            public void mouseClicked(MouseEvent e) {
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
             */
            public void mouseEntered(MouseEvent e) {
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
             */
            public void mouseExited(MouseEvent e) {
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
             */
            public void mouseReleased(MouseEvent e) {
            }

        });

        // Shows a nice icon for drag and drop but doesn't import anything
        setTransferHandler(new TransferHandler() {
            public boolean canImport(JComponent comp, DataFlavor[] flavors) {
                return true;
            }
        });
    }

    /**
     *
     */
    public void clearSelection() {
        setSelectionEntry(null, null);
    }

    /**
     *
     */
    public void setSelectionEntry(JLabel entry, mxGraphTransferable t) {
        JLabel previous = selectedEntry;
        selectedEntry = entry;

        if (previous != null) {
            previous.setBorder(null);
            previous.setOpaque(false);
        }

        //if (selectedEntry != null) {
        //    selectedEntry.setBorder(BorderFactory.createLineBorder(Color.black));
         //   selectedEntry.setOpaque(true);
        //}

        eventSource.fireEvent(new mxEventObject(mxEvent.SELECT, "entry",
                selectedEntry, "transferable", t, "previous", previous));
    }

    /**
     *
     */
    public void setPreferredWidth(int width) {
        if(width>200)
        {
            d.setSize(width-20,width-20);
            for(int i=0;i<elements.size();++i){
                label = elements.get(i);
                Icon icon = icons.get(i);
                label.setPreferredSize(d);
                BufferedImage bi = new BufferedImage( icon.getIconWidth(), icon.getIconHeight(),BufferedImage.TYPE_INT_ARGB);
                Graphics g = bi.createGraphics();
                icon.paintIcon(null, g, 0,0); 
                BufferedImage img = resizeImg(bi,width-100,width-100);
                label.setIcon(new ImageIcon(img));
                g.dispose();
                bi = null;
            }
        }
    }
	
    public BufferedImage resizeImg(BufferedImage image, int width, int height) {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = (Graphics2D) bi.createGraphics(); 
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)); 
        g2d.drawImage(image, 0, 0, width, height, null); 
        g2d.dispose();
        return bi;
    }

    /**
     *
     * @param name
     * @param icon
     * @param style
     * @param width
     * @param height
     * @param value
     */
    public void addEdgeTemplate(final String name, ImageIcon icon,
            String style, int width, int height, Object value) {
        mxGeometry geometry = new mxGeometry(0, 0, width, height);
        geometry.setTerminalPoint(new mxPoint(0, height), true);
        geometry.setTerminalPoint(new mxPoint(width, 0), false);
        geometry.setRelative(true);

        mxCell cell = new mxCell(value, geometry, style);
        cell.setEdge(true);
        
        addTemplate(name, icon, cell);
    }
    
    /**
     *
     * @param name
     * @param icon
     * @param style
     * @param width
     * @param height
     * @param value
     */
    public void addTemplate(final String name, ImageIcon icon, String style,
            int width, int height, Object value) {
        mxCell cell = new mxCell(value, new mxGeometry(0, 0, width, height),
                style);
        cell.setVertex(true);
        addTemplate(name, icon, cell);
    }

    /**
     *
     * @param name
     * @param icon
     * @param cell
     */
    public void addTemplate(final String name, ImageIcon icon, mxCell cell) {
        mxRectangle bounds = (mxGeometry) cell.getGeometry().clone();
        final mxGraphTransferable t = new mxGraphTransferable(
                new Object[]{cell}, bounds);

        // Scales the image if it's too large for the library
        if (icon != null)
        {
            int heigth = icon.getIconHeight();
            int wight = icon.getIconWidth();
            if (heigth > 150)
            {
		heigth = 150;
            }
            if(wight > 150)
            {
                wight = 150;
            }
            icon = new ImageIcon(icon.getImage().getScaledInstance(wight, heigth,java.awt.Image.SCALE_SMOOTH));
        }

        icons.add(icon);
        
        JLabel entry = new JLabel(icon);
        entry.setPreferredSize(new Dimension(150, 150));
        entry.setBackground(Palette.this.getBackground().brighter());
        entry.setFont(new Font(entry.getFont().getFamily(), 0, 18));
        entry.setVerticalTextPosition(JLabel.TOP);
        entry.setHorizontalTextPosition(JLabel.CENTER);
        entry.setIconTextGap(0);
        entry.setToolTipText(name);
        entry.setText(name);

        entry.addMouseListener(new MouseListener() {

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
             */
            public void mousePressed(MouseEvent e) {
                setSelectionEntry(entry, t);
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
             */
            public void mouseClicked(MouseEvent e) {
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
             */
            public void mouseEntered(MouseEvent e) {
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
             */
            public void mouseExited(MouseEvent e) {
            }

            /*
             * (non-Javadoc)
             * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
             */
            public void mouseReleased(MouseEvent e) {   
            }

        });

        // Install the handler for dragging nodes into a graph
        DragGestureListener dragGestureListener = new DragGestureListener() {
            /**
             *
             */
            public void dragGestureRecognized(DragGestureEvent e) {
                e
                        .startDrag(null, mxSwingConstants.EMPTY_IMAGE, new Point(),
                                t, null);
            }

        };

        DragSource dragSource = new DragSource();
        dragSource.createDefaultDragGestureRecognizer(entry,
                DnDConstants.ACTION_COPY, dragGestureListener);

        add(entry);
        elements.add(entry);
    }

    /**
     * @param eventName
     * @param listener
     * @see com.mxgraph.util.mxEventSource#addListener(java.lang.String,
     * com.mxgraph.util.mxEventSource.mxIEventListener)
     */
    public void addListener(String eventName, mxIEventListener listener) {
        eventSource.addListener(eventName, listener);
    }

    /**
     * @return whether or not event are enabled for this palette
     * @see com.mxgraph.util.mxEventSource#isEventsEnabled()
     */
    public boolean isEventsEnabled() {
        return eventSource.isEventsEnabled();
    }

    /**
     * @param listener
     * @see
     * com.mxgraph.util.mxEventSource#removeListener(com.mxgraph.util.mxEventSource.mxIEventListener)
     */
    public void removeListener(mxIEventListener listener) {
        eventSource.removeListener(listener);
    }

    /**
     * @param eventName
     * @param listener
     * @see com.mxgraph.util.mxEventSource#removeListener(java.lang.String,
     * com.mxgraph.util.mxEventSource.mxIEventListener)
     */
    public void removeListener(mxIEventListener listener, String eventName) {
        eventSource.removeListener(listener, eventName);
    }

    /**
     * @param eventsEnabled
     * @see com.mxgraph.util.mxEventSource#setEventsEnabled(boolean)
     */
    public void setEventsEnabled(boolean eventsEnabled) {
        eventSource.setEventsEnabled(eventsEnabled);
    }

}
