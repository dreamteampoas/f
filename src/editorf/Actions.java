/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package editorf;

import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.canvas.mxSvgCanvas;
import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxGdCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxDomUtils;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.view.mxGraph;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.w3c.dom.Document;

/**
 *
 * @author user
 */
public class Actions {
    
    /**
     * 
     * @param e
     * @return Returns the graph for the given action event.
     */
    public static final EditorF getEditor(ActionEvent e)
    {
        if (e.getSource() instanceof Component)
        {
            Component component = (Component) e.getSource();

            while (component != null
                            && !(component instanceof EditorF))
            {
                component = component.getParent();
            }

            return (EditorF) component;
        }

        return null;
    }
        
    /**
     *
     */
    @SuppressWarnings("serial")
    public static class CreateAction extends AbstractAction
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            EditorF editor = getEditor(e);

            if (editor != null)
            {
                if (!editor.isModified()
                                || JOptionPane.showOptionDialog(editor, "Несохраненные изменения "
                                        + "будут утеряны.\nПродолжить?",
                                        "Несохраненные изменения", 
                                        JOptionPane.YES_NO_CANCEL_OPTION, 
                                        JOptionPane.INFORMATION_MESSAGE, 
                                        null, 
                                        new Object[]{"Да", "Нет", "Отмена"}, 
                                        "Да") == JOptionPane.YES_OPTION)
                {
                    mxGraph graph = editor.getGraphComponent().getGraph();

                    // Check modified flag and display save dialog
                    mxCell root = new mxCell();
                    root.insert(new mxCell());
                    graph.getModel().setRoot(root);

                    editor.setModified(false);
                    editor.setCurrentFile(null);
                    editor.getGraphComponent().zoomAndCenter();
                }
            }
        }
    }
    
    /**
     *
     */
    @SuppressWarnings("serial")
    public static class OpenAction extends AbstractAction
    {
        /**
         * 
         */
        protected String lastDir;

        /**
         * 
         */
        protected void resetEditor(EditorF editor)
        {
            editor.setModified(false);
            editor.getUndoManager().clear();
            editor.getGraphComponent().zoomAndCenter();
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            EditorF editor = getEditor(e);

            if (editor != null)
            {
                if (!editor.isModified()
                                || JOptionPane.showOptionDialog(editor, "Несохраненные изменения "
                                        + "будут утеряны.\nПродолжить?",
                                        "Несохраненные изменения", 
                                        JOptionPane.YES_NO_CANCEL_OPTION, 
                                        JOptionPane.INFORMATION_MESSAGE, 
                                        null, 
                                        new Object[]{"Да", "Нет", "Отмена"}, 
                                        "Да") == JOptionPane.YES_OPTION)
                {
                    mxGraph graph = editor.getGraphComponent().getGraph();

                    if (graph != null)
                    {
                        String wd = (lastDir != null) ? lastDir : System
                                        .getProperty("user.dir");

                        JFileChooser fc = new JFileChooser(wd);
                        EditorFileFilter filter = new EditorFileFilter(".edf", "EditorF файл (.edf)");
                        fc.addChoosableFileFilter(filter);
                        fc.setFileFilter(filter);
                        
                        int rc = fc.showDialog(null, "Открыть");
                        
                        if (rc == JFileChooser.APPROVE_OPTION)
                        {
                            lastDir = fc.getSelectedFile().getParent();

                            try
                            {
                                Document document = mxXmlUtils
                                                .parseXml(mxUtils.readFile(fc
                                                                .getSelectedFile()
                                                                .getAbsolutePath()));
                            
                                mxCodec codec = new mxCodec(document);
                                codec.decode(document.getDocumentElement(),
                                            graph.getModel());
                                
                                editor.setCurrentFile(fc
                                                .getSelectedFile());
                                

                                resetEditor(editor);
                            }
                            catch (Exception ex)
                            {
                                ex.printStackTrace();
                                JOptionPane.showMessageDialog(
                                                editor.getGraphComponent(),
                                                ex.toString(),
                                                "Произошла ошибка при открытии файла.",
                                                JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     *
     */
    @SuppressWarnings("serial")
    public static class SaveAction extends AbstractAction
    {  
        /**
         * 
         */
        protected boolean showDialog;        
        protected boolean toImage;


        /**
         * 
         */
        protected String lastDir = null;
        
        /**
         * 
         */
        public SaveAction(boolean showDialog)
        {
            this.showDialog = showDialog;
        }
        
        /**
         * 
         */
        public SaveAction(boolean showDialog, boolean toImage)
        {
            this.showDialog = showDialog;
            this.toImage = toImage;
        }
                
        @Override
        public void actionPerformed(ActionEvent e) {
            EditorF editor = getEditor(e);

            if (editor != null)
            {
                mxGraphComponent graphComponent = editor.getGraphComponent();
                mxGraph graph = graphComponent.getGraph();
                FileFilter selectedFilter = null;
                EditorFileFilter filter;
                if (this.toImage)
                    filter = new EditorFileFilter(".bmp", "BMP файл (.bmp)");
                else
                    filter = new EditorFileFilter(".edf", "EditorF файл (.edf)");
                String filename = null;

                if (showDialog || editor.getCurrentFile() == null)
                {
                    String wd;

                    if (lastDir != null)
                    {
                        wd = lastDir;
                    }
                    else if (editor.getCurrentFile() != null)
                    {
                        wd = editor.getCurrentFile().getParent();
                    }
                    else
                    {
                        wd = System.getProperty("user.dir");
                    }

                    JFileChooser fc = new JFileChooser(wd);

                    // Adds the default file format
                    fc.addChoosableFileFilter(filter);
                    fc.setFileFilter(filter);
                    int rc = fc.showDialog(null, "Сохранить");

                    if (rc != JFileChooser.APPROVE_OPTION)
                    {
                        return;
                    }
                    else
                    {
                        lastDir = fc.getSelectedFile().getParent();
                    }

                    filename = fc.getSelectedFile().getAbsolutePath();
                    selectedFilter = fc.getFileFilter();

                    if (selectedFilter instanceof EditorFileFilter)
                    {
                        String ext = ((EditorFileFilter) selectedFilter)
                                        .getExtension();

                        if (!filename.toLowerCase().endsWith(ext))
                        {
                            filename += ext;
                        }
                    }

                    if (new File(filename).exists()
                                    && JOptionPane.showOptionDialog(graphComponent, "Переписать существующий файл?",
                                        "Перезапись файла", 
                                        JOptionPane.YES_NO_CANCEL_OPTION, 
                                        JOptionPane.INFORMATION_MESSAGE, 
                                        null, 
                                        new Object[]{"Да", "Нет", "Отмена"}, 
                                        "Да") == JOptionPane.YES_OPTION)
                    {
                        return;
                    }
                }
                else
                {
                    filename = editor.getCurrentFile().getAbsolutePath();
                }

                try
                {
                    String ext = filename.substring(filename.lastIndexOf('.') + 1);

                    if (ext.equalsIgnoreCase("edf"))
                    {
                        mxCodec codec = new mxCodec();
                        String xml = mxXmlUtils.getXml(codec.encode(graph
                                        .getModel()));

                        mxUtils.writeFile(xml, filename);

                        editor.setModified(false);
                        editor.setCurrentFile(new File(filename));
                    }
                    else
                    {
                        BufferedImage image = mxCellRenderer
                                        .createBufferedImage(graph, null, 1, 
                                        graphComponent.getBackground(),
                                        graphComponent.isAntiAlias(), null,
                                        graphComponent.getCanvas());

                        if (image != null)
                        {
                            ImageIO.write(image, ext, new File(filename));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(graphComponent, "Нечего экспортировать.");
                        }
                    }
                }
                catch (Throwable ex)
                {
                    JOptionPane.showMessageDialog(graphComponent,
                                    ex.toString(), "Произошла ошибка при сохранении в файл.",
                                    JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    
    /**
     *
     */
    @SuppressWarnings("serial")
    public static class ExitAction extends AbstractAction
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            EditorF editor = getEditor(e);

            if (editor != null && (!editor.isModified()
                                || JOptionPane.showOptionDialog(editor, "Несохраненные изменения "
                                        + "будут утеряны.\nПродолжить?",
                                        "Несохраненные изменения", 
                                        JOptionPane.YES_NO_CANCEL_OPTION, 
                                        JOptionPane.INFORMATION_MESSAGE, 
                                        null, 
                                        new Object[]{"Да", "Нет", "Отмена"}, 
                                        "Да") == JOptionPane.YES_OPTION))
            {
                editor.exit();
            }
        }
    }

    /**
     *
     */
    @SuppressWarnings("serial")
    public static class HistoryAction extends AbstractAction
    {
        /**
         * 
         */
        protected boolean undo;

        /**
         * 
         */
        public HistoryAction(boolean undo)
        {
            this.undo = undo;
        }

        /**
         * 
         */
        public void actionPerformed(ActionEvent e)
        {
            EditorF editor = getEditor(e);

            if (editor != null)
            {
                    if (undo)
                    {
                            editor.getUndoManager().undo();
                    }
                    else
                    {
                            editor.getUndoManager().redo();
                    }
            }
        }
    }
}
